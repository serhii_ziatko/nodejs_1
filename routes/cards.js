var express = require('express');
var router = express.Router();
var json =  require('../db.json'); 
let count = 0;


/* Add Id */
const counter = (arr) => {
  while( true ){
    count++;
    var i = arr.findIndex( item => item.id === count);
    console.log( i );
    if ( i < 0 ){
      break;
    }
  }
  return count;
}

/* GET cards */
router.get('/', function(req, res, next) {
  res.json( json.cards );
});

/* GET cards.id */
router.get('/:id', function(req, res, next) {
  const index = json.cards.findIndex(item => item.id == req.params.id);
  if( json.cards[index] ){
    res.json(json.card[index]);
  }
});

/* POST cards.id */
router.post('/', function(req, res, next) {
  let id = counter(json.cards);
  console.log( id );
  req.body.id = id;
  req.body.title = `${req.body.title} ${id}`; 
  json.cards.push(req.body);
  res.json( json.cards ); 
});

/* DELETE cards.id */
router.delete('/:id', function(req, res, next) {
  const index = json.cards.findIndex(item => item.id == req.params.id );
  if( json.cards[index] ){
    json.cards.splice(index, 1);
  }
  res.json( json.cards );
});


/* PUT cards.id */
router.put('/:id', function(req, res, next) {
  const index = json.cards.findIndex(item => item.id == req.params.id );
  if( json.cards[index] ){
    json.cards[index].todos.length = 0;
    Object.assign( json.cards[index].todos, req.body.todos );
  }
  res.json( json.cards );
});


module.exports = router;
 